FROM ubuntu:latest

# File Author / Maintainer
MAINTAINER huezohuezo1990
ENV DEBIAN_FRONTEND noninteractive
ENV TZ=America/El_Salvador

# Set the timezone.
ENV TZ=America/El_Salvador
RUN apt-get update 
RUN apt-get install -y tzdata 
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime 
RUN echo $TZ > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata


ENV JAVA_PKG=jdk-17*.tar.gz 
ENV JAVA_HOME=/usr/java/default


ADD $JAVA_PKG /usr/java/


RUN export JAVA_DIR=$(ls -1 -d /usr/java/*) && \
    ln -s $JAVA_DIR /usr/java/latest && \
    ln -s $JAVA_DIR /usr/java/default && \
    update-alternatives --install /usr/bin/java java $JAVA_DIR/bin/java 20000 && \
    update-alternatives --install /usr/bin/javac javac $JAVA_DIR/bin/javac 20000 && \
    update-alternatives --install /usr/bin/jar jar $JAVA_DIR/bin/jar 20000

